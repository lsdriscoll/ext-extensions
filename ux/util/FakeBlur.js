Ext.define('Ext.ux.util.FakeBlur',{

    mixins: {
        observable: 'Ext.util.Observable'
    },

    /**
     * @cfg {[Ext.Component]} blurTargets An array of additional components that contribute to the blur status of this
     * component
     */

    constructor: function(config){
        if(!this.events)
            Ext.Error.raise('Component events have not yet been initialiazed, please call the constructor for FakeBlur after the Component\'s call to it\'s parent\'s constructor');

        Ext.apply(this, config);

        if(!this.rendered)
            this.on('boxready', this.setupFakeBlur, this);
        else
            this.setupFakeBlur();
    },

    /**
     * @private
     */
    setupFakeBlur: function(){
        this.blurObject = {};
        this.blurTargets = this.blurTargets || [];
        this.blurTargets.push(this);

        for(var i= 0, len= this.blurTargets.length; i<len; i++){
            this.blurObject[this.blurTargets[i].el.id] = this.blurTargets[i].el.getRegion();
            this.blurRegion = !this.blurRegion ? this.blurObject[this.blurTargets[i].el.id] : this.blurRegion.union(this.blurObject[this.blurTargets[i].el.id]);
            this.blurTargets[i].on('move', this.updateBlurRegion, this);
        }

        // create a managed listener using a DOM event (performance)
        this.mon(Ext.getBody(), 'click', this.testFakeBlur, this, {
            normalize: false
        });
    },

    /**
     * @private
     */
    updateBlurRegion: function(cmp){
        this.blurObject[cmp.el.id] = cmp.el.getRegion();
        this.blurRegion = null;

        for(var i= 0, len= this.blurTargets.length; i<len; i++)
            this.blurRegion = !this.blurRegion ? this.blurObject[this.blurTargets[i].el.id] : this.blurRegion.union(this.blurObject[this.blurTargets[i].el.id]);
    },

    /**
     * @private
     */
    testFakeBlur: function(e){

        // We fire a blur event if the click event is outside of the allowed Ext.util.Region
        // We supress the event if this Ext.Component is not visible as focus is considered impossible

        if(!this.blurRegion.contains(e.getPoint()) && this.isVisible())
            this.fireEvent('blur');
    }
});