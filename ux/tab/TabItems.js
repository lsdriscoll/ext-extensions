/**
 * @author Lee Driscoll
 *
 * A plugin class to add an __items__ config to an Ext.tab.Tab.
 * Define the items as if it were a container. e.g. items: [myComponentA, myComponentB].
 * If you define an action property on an item as a function, it will be called when the item is clicked on.
 */
Ext.define('Ext.ux.tab.TabItems',{
    extend: 'Ext.AbstractPlugin',

    alias: 'plugin.tabitems',

    init: function(tab){
        var fn;

        tab.defaults = tab.defaults || {};

        fn = function(){
            var xtype, item, cfg;

            cfg = Ext.Object.merge({},{
                cls: 'tab-item',
                style:{
                    verticalAlign: 'top',
                    display     : 'inline-block',
                    marginLeft  : '6px'
                },
                height: tab.btnWrap.first().getHeight()
            },tab.defaults);

            for(var i= 0,len= tab.items.length; i<len; i++) {

		item = Ext.ComponentManager.create(Ext.Object.merge({}, cfg, tab.items[i]));

		item.on('render', function () {
		Ext.Array.replace(tab.items, i, 1, item);
		if (item.action && Ext.isFunction(item.action))
			item.el.on('click', function (e) {
			e.stopEvent();
			item.action();
			}, item);
		}, this);
		item.render(tab.btnWrap);

                tab.btnWrap.setStyle({lineHeight:'normal'});
            }
        };

        tab.on('boxready',fn,this);
    }

});